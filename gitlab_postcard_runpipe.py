from webexteamssdk import WebexTeamsAPI
from webexteamssdk.models.cards.card import AdaptiveCard
from webexteamssdk.models.cards.inputs import Toggle, Text
from webexteamssdk.models.cards.components import TextBlock, Image
from webexteamssdk.models.cards.actions import Submit
from webexteamssdk.utils import make_attachment
from webexteamssdk.models.cards.options import ImageSize

room_id = "Y2lzY29zcGFyazovL3VzL1JPT00vNGM5MjY5NzAtZGM1NC0xMWVjLTg2NWItYmJhYTc3ZmE1ZTU0"

pipe_img = Image(url='https://img.icons8.com/external-phatplus-lineal-color-phatplus/344/external-pipeline-bathroom-phatplus-lineal-color-phatplus-2.png',
    size=ImageSize("small"), separator=True)
greeting = TextBlock("Merge Complete, Run Pipeline?")
feature_toggle = Toggle(title="Feature Template Update?",id="feature_update", valueOn="true", valueOff="false")
device_toggle = Toggle(title="Device Template Update?",id="device_update", valueOn="true", valueOff="false")
attach_toggle = Toggle(title="Device Variable Update?",id="attach_device", valueOn="true", valueOff="false")
target_text = Text(id="target_var", placeholder="Enter Target Inventory Host/Group")

submit = Submit(title="Start Pipeline", data={"callback_keyword": "sdwan_pipe"})

card = AdaptiveCard(body=[pipe_img, greeting, feature_toggle, device_toggle, attach_toggle, target_text], actions=[submit])

wxapi = WebexTeamsAPI()
wxapi.messages.create(text="fallback", roomId=room_id, attachments=[make_attachment(card)])